# Generated by Django 4.1.5 on 2023-01-18 17:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("todos", "0002_rename_todo_todolist"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todolist",
            name="name",
            field=models.CharField(max_length=100),
        ),
        migrations.CreateModel(
            name="TodoItem",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("task", models.CharField(max_length=100)),
                ("due_date", models.DateTimeField(blank=True, null=True)),
                ("is_completed", models.BooleanField(default=False, null=True)),
                (
                    "list",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="todoitem",
                        to="todos.todolist",
                    ),
                ),
            ],
        ),
    ]
