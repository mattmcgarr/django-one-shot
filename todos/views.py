from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list": todo_list}
    return render(request, "todos/todos.html", context)


def todo_list_detail(request, id):
    todo_tasks = TodoList.objects.get(id=id)
    context = {"todo_list": todo_tasks}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    form = TodoListForm()
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    update_todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=update_todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=update_todo)
    context = {"todo_list": update_todo, "form": form}
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    delete_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm()
    return render(request, "todos/createitem.html", {"form": form})


def todo_item_update(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm(instance=todoitem)
    return render(request, "todos/edititem.html", {"form": form})
